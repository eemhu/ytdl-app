// Renderer.js

// Since the electron building process doesn't change in-code javascript paths,
// we need to check if the require fails - and depending on that,
// we can load the jquery js file from the appropriate location.
var jquery_module = {};
try {
    jquery_module = require("./resources/app/lib/jquery/3.6.0/js/jquery-3.6.0.min.js");
}
catch (err) {
    if (err instanceof Error && err.code === "MODULE_NOT_FOUND") {
        console.log("Error loading jQuery from extraResources, attempting to use dev location");
        jquery_module = require("./lib/jquery/3.6.0/js/jquery-3.6.0.min.js");
    }
    else {
        throw err;
    }
}

// set $ and jQuery to the jQuery module
window.$ = window.jQuery = jquery_module;
// import electron
const electron = require('electron');
// import socket.io
var socket = io();

// fetch UI items into javascript
const btnSubmitVideoDownload = document.getElementById("submitVideoDownload");
const btnPasteUrl = document.getElementById("pasteUrl");
const txtVideoUrl = document.getElementById("youtubeUrlInput");
const selectQuality = document.getElementById("qualitySelect");
const progressText = document.getElementsByClassName("pg-text")[0];

// Fires on event "progress", i.e. when the server emits the progress
socket.on('progress', function(prog) {
    progressText.innerHTML = "Downloading: " + prog.progress + " %";
});

// Pick folder when "Download" is pressed
btnSubmitVideoDownload.addEventListener("click", function(e) {
    e.preventDefault();

    pickFolder();
});

// Paste clipboard content on click (paste button next to URL textbox)
btnPasteUrl.addEventListener("click", function(e) {
    e.preventDefault();

    txtVideoUrl.value = electron.clipboard.readText();
});

// Invokes the native SaveFileDialog and after the location has been selected,
// starts the download by sending the request to the server
function pickFolder() {
    let dialog = electron.remote.dialog;
    let win = electron.remote.getCurrentWindow();

    let options = {
        title : "Save",
        defaultPath : "C:\\",
        buttonLabel : "Save",
        filters : [
            {name: 'mp4 file', extensions: ['mp4']},
            {name: 'm4a file', extensions: ['m4a']},
            {name: 'All files', extensions: ['*']}
        ],
        properties: ['showOverwriteConfirmation']
    };

    dialog.showSaveDialog(win, options).then((file) => {
        if (!file.canceled) processDownload(file.filePath);
    });
}

// Process the download by changing select value to string
// and sending HTTP POST
// Also manipulates UI to show progress graphic
function processDownload(path) {
    let quality = "";
    switch (selectQuality.value) {
        case "0":
            quality = "highest"; break;
        case "1":
            quality = "highestvideo"; break;
        case "2":
            quality = "highestaudio"; break;
        case "3":
            quality = "lowestaudio"; break;
        default:
            quality = "highest"; break;
    }

    const dataToSend = {
        url : txtVideoUrl.value,
        localPath : path,
        quality : quality
    };

    $(".progressGraphic").prop("hidden", false);
    $("#submitVideoDownload").prop("disabled", true);

    $.post('/initDownload', dataToSend,  data => {
        console.log("send");
    }).done(data => {
        $(".progressGraphic").prop("hidden", true);
        $("#submitVideoDownload").prop("disabled", false);
        window.alert(data);
        progressText.innerHTML = "Preparing download...";
    });
}