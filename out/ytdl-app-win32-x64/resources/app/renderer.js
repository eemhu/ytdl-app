// window.$ = window.jQuery = require("./resources/app/lib/jquery/3.6.0/js/jquery-3.6.0.min.js");

var jquery_module = {};

try {
    jquery_module = require("./resources/app/lib/jquery/3.6.0/js/jquery-3.6.0.min.js");
}
catch (err) {
    if (err instanceof Error && err.code === "MODULE_NOT_FOUND") {
        console.log("Error loading jQuery from extraResources, attempting to use dev location");
        jquery_module = require("./lib/jquery/3.6.0/js/jquery-3.6.0.min.js");
    }
    else {
        throw err;
    }
}

window.$ = window.jQuery = jquery_module;
const electron = require('electron');

var socket = io();

// const form = document.getElementById("downloadVideoForm");
const btnSubmitVideoDownload = document.getElementById("submitVideoDownload");
const btnPasteUrl = document.getElementById("pasteUrl");
const txtVideoUrl = document.getElementById("youtubeUrlInput");
//const txtLocalPath = document.getElementById("savePathInput");
const selectQuality = document.getElementById("qualitySelect");
const progressText = document.getElementById("progressGraphic-text");
let files = '';

/*txtLocalPath.addEventListener("change", function(e) {
    files = e.target.files;
});*/

socket.on('progress', function(prog) {
    progressText.innerHTML = "Downloading: " + prog.progress + " %";
});

btnSubmitVideoDownload.addEventListener("click", function(e) {
    e.preventDefault();

    pickFolder();
});

btnPasteUrl.addEventListener("click", function(e) {
    e.preventDefault();

    txtVideoUrl.value = electron.clipboard.readText();
});

function pickFolder() {
    let dialog = electron.remote.dialog;
    let win = electron.remote.getCurrentWindow();

    let options = {
        title : "Save",
        defaultPath : "C:\\",
        buttonLabel : "Save",
        filters : [
            {name: 'mp4 file', extensions: ['mp4']},
            {name: 'm4a file', extensions: ['m4a']},
            {name: 'All files', extensions: ['*']}
        ],
        properties: ['showOverwriteConfirmation']
    };

    dialog.showSaveDialog(win, options).then((file) => {
        processDownload(file.filePath);
    });
}

function processDownload(path) {
    if (path === undefined || path.length < 1) {
        return;
    }

    let quality = "";
    switch (selectQuality.value) {
        case "0":
            quality = "highest"; break;
        case "1":
            quality = "highestvideo"; break;
        case "2":
            quality = "highestaudio"; break;
        case "3":
            quality = "lowestaudio"; break;
        default:
            quality = "highest"; break;
    }

    const dataToSend = {
        url : txtVideoUrl.value,
        localPath : path,
        quality : quality
    };

    $("#progressGraphic").prop("hidden", false);
    $("#submitVideoDownload").prop("disabled", true);

    $.post('/initDownload', dataToSend,  data => {
        console.log("send");
    }).done(data => {
        $("#progressGraphic").prop("hidden", true);
        $("#submitVideoDownload").prop("disabled", false);
        window.alert(data);
        progressText.innerHTML = "Preparing download...";
    });
}