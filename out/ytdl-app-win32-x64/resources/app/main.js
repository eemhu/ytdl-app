const { app, BrowserWindow, remote, dialog, Menu } = require('electron');
const path = require('path');

const process = require('process');

var enableDevConsole = false;
var enableMenuBar = false;
process.argv.forEach((arg, i, arr) => {
    if (i === 0 || i === 1) {
        /* ignore node path and script path */
    }
    else if (arg === "--enable_dev_console") {
        enableDevConsole = true;
    }
    else if (arg === "--enable_menu_bar") {
        enableMenuBar = true;
    }
});

const http = require('http');
const express = require('express');
const cors = require('cors');
const PORT = 7304;
const expressApp = express();

expressApp.use(cors());
expressApp.use(express.json());
expressApp.use(express.urlencoded({
    extended: true
}));

expressApp.use(express.static(path.join(__dirname, "./")));

const { Server } = require('socket.io');
const server = http.createServer(expressApp);
const io = new Server(server);

const fs = require('fs');

const ytdl = require('ytdl-core');



function createWindow() {
    if (!enableMenuBar) Menu.setApplicationMenu(null);

    const win = new BrowserWindow({
        width: 1024,
        height: 450,
        maximizable: false,
        fullscreenable: false,
        resizable: false,
        webPreferences: {
            enableRemoteModule: true,
            preload: path.join(__dirname, 'preload.js'),
            nodeIntegration: true,
            contextIsolation: false,
            devTools: enableDevConsole
        }
    });

    win.loadURL(`http://localhost:${PORT}`);
    if (enableDevConsole) win.webContents.openDevTools();
}

app.whenReady().then(() => {
    createWindow();

    app.on('activate', function() {
        if (BrowserWindow.getAllWindows().length === 0) createWindow();
    });
});

app.on('window-all-closed', function() {
    if (process.platform !== 'darwin') app.quit();
});



expressApp.post('/initDownload', (req, res) => {
    console.log(req.body);

    const urlToDownload = req.body.url;
    var localPath = req.body.localPath;
    const quality = req.body.quality;
    var options = { quality : quality }; // quality === 0

    //console.log("Quality: " + quality);
    //console.log(options.quality);
    const video = ytdl(urlToDownload, options);
    video.pipe(fs.createWriteStream(localPath));

    let previousProgress = 0;
    video.on('progress', (chunkLength, downloaded, total) => {
        const percent = downloaded / total;
        //console.log("Percent DL'd: " + percent);

        if (percent - previousProgress >= 0.01) {
            io.emit('progress', { progress : Math.round(percent * 100) });
            previousProgress = percent;
        }
        
        if (percent >= 1) {
            io.emit('progress', { progress : Math.round(percent * 100) });
            res.send("Download complete, check " + localPath + " for your file.");
        }
    });
});

io.on('connection', (socket) => {
    console.log("A user connected");
});

server.listen(process.env.PORT || PORT, () => {
    console.log(`Running on port ${process.env.PORT || PORT}`);
});


// ffmpeg
/*const cp = require('child_process');
const readline = require('readline');
const ffmpeg = require('ffmpeg-static');
const tracker = {
    start: Date.now(),
    audio: { downloaded: 0, total: Infinity },
    video: { downloaded: 0, total: Infinity },
    merged: { frame: 0, speed: '0x', fps: 0 },
};
*/
